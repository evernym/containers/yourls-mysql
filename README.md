# yourls-mysql

Yourls image with mysql database

## Environment variables for this image

-e YOURLS_COOKIEKEY=... (default to unique random SHA1s)
-e YOURLS_SITE=... (yourls instance url)
-e YOURLS_USER=... (yourls instance user name)
-e YOURLS_PASS=... (yourls instance user password)
